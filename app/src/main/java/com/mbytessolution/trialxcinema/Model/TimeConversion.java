package com.mbytessolution.trialxcinema.Model;

import java.util.ArrayList;
import java.util.List;

public class TimeConversion {

    public static void main(String[] args) {

//        String s = "07:45:54PM";
//        String result = timeConversion(s);
//        System.out.println(result);
//
//        List<Integer> scores = new ArrayList<>();
//        scores.add(5);
//        scores.add(10);
//        scores.add(2);
//        scores.add(4);
//
//        for (Integer integer:   breakingRecords(scores)) {
//            System.out.println(integer);
//        }

        List<Integer> numberList = new ArrayList<>();

        numberList.add(1);
        numberList.add(3);
        numberList.add(2);
        numberList.add(6);
        numberList.add(1);
        numberList.add(2);

        System.out.println(divisibleSumPairs(numberList.size(), 3, numberList));





    }

    public static String timeConversion(String s) {

        String[] time = s.split(":");
        String s1 = time[0];
        StringBuilder result = new StringBuilder();

        if(s.contains("PM") && !time[0].equals("12")) {
            s1 = String.valueOf(Integer.parseInt(s1) + 12);
        }

        if (s.contains("AM") && time[0].equals("12")) {
            s1 = "00";
        }

        result.append(s1).append(":");

        for (int i=1; i<time.length; i++) {
            if (i == time.length-1) {
                result.append(time[i].replaceAll("AM", "").replaceAll("PM", ""));
            }
            else {
                result.append(time[i].replaceAll("AM", "").replaceAll("PM", "")).append(":");
            }

        }

        return result.toString();

    }

    public static List<Integer> breakingRecords(List<Integer> scores) {

        List<Integer> minMaxList = new ArrayList<>(2);
        int maxValue = scores.get(0);
        int minValue = scores.get(0);

        int maxRecord = 0;
        int minRecord = 0;

        for (int i=1; i<scores.size(); i++) {
            if (scores.get(i) > maxValue) {
                maxValue = scores.get(i);
                maxRecord++;
            }
            else if (scores.get(i) < minValue) {
                minValue = scores.get(i);
                minRecord++;
            }
        }

        minMaxList.add(maxRecord);
        minMaxList.add(minRecord);

        return minMaxList;


    }

    public static int divisibleSumPairs(int n, int k, List<Integer> ar) {

        int result = 0;

        for (int i=0; i<ar.size(); i++) {
            for (int j=i+1; j<ar.size(); j++) {
                if ((ar.get(i) + ar.get(j))%k == 0) {
                 result++;
                }

            }
        }

        return result;

    }


}
